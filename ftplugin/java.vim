augroup ft_java
  autocmd! * <buffer>
augroup end

" packadd vim-classpath

setlocal omnifunc=javacomplete#Complete
