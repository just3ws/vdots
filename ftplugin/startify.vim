augroup ft_startify
  autocmd! * <buffer>
augroup end

setlocal cursorline
setlocal foldcolumn=0
setlocal nofoldenable
setlocal nolist
setlocal signcolumn=no
