augroup ft_yaml
  autocmd! * <buffer>
augroup end

setlocal expandtab
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal tabstop=2
