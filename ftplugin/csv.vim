augroup ft_csv
  autocmd! * <buffer>
augroup end

" augroup CSV_Editing
"   autocmd!
"   autocmd BufRead,BufWritePost *.csv :%ArrangeColumn
"   autocmd BufWritePre *.csv :%UnArrangeColumn
" augroup end
"
" let g:csv_autocmd_arrange	   = 1
" let g:csv_autocmd_arrange_size = 1024*1024
"
" let g:csv_delim = ','
" let g:csv_delim_test = ','
" let g:csv_strict_columns = 1
