augroup ft_sass
  autocmd! * <buffer>
augroup end

let g:ale_fixers.sass = ['prettier']
