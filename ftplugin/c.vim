augroup ft_c
  autocmd! * <buffer>
augroup end

setlocal omnifunc=ccomplete#Complete
setlocal shiftwidth=4
setlocal softtabstop=4
